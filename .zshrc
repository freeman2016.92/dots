#!/usr/bin/zsh

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx &> /dev/null

export HISTFILE=~/.zhistory
export HISTSIZE=3000
export SAVEHIST=3000

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

#bindkey -e
bindkey    "^[[3~"          delete-char
#bindkey    "^[3;5~"         delete-char


### ALIASES ###
# spark aliases
alias cl="clear"

# navigation
alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# broot
alias br='broot -dhp'
alias bs='broot --sizes'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing

# pacman and yaourt
#alias spu="sudo pacman -Syu"
#alias sp="sudo pacman -S"
#alias spr="sudo pacman -Rns"
#alias spc="sudo pacman -Sc"
#alias spo="sudo pacman -Rns (pacman -Qtdq && pacman -Qdttq)"
alias neo="neofetch --off"
alias n="neofetch"
alias m="mocp"
alias mx="mocp -x"
alias r="ranger"
alias p="pfetch"
alias u="ufetch"
alias h="htop"
#alias b="bashtop"
#alias yu="yay -Syu"
#alias y="yay"
#alias ys="yay -S"
#alias yc="yay -Scc"
#alias yrsn="yay -Rsn"
alias xbi="sudo xbps-install -S"
alias xbr="sudo xbps-remove -Rf"
alias xbo="sudo xbps-remove -Oo"
alias xbu="sudo xbps-install -Suv"
alias xbq="xbps-query -Rs"
alias rm="rm -rf"
alias wtr="curl 'wttr.in/361203?lang=ru'"
alias moon="curl wttr.in/Moon"
alias s="speedtest"
alias torrserver="./torrserver"
alias upgrub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias pl='ncmpcpp'
alias player='ncmpcpp -S visualizer'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
#alias lynx='lynx -cfg=~/.lynx/lynx.cfg -lss=~/.lynx/lynx.lss -vikeys'


## get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'


# Merge Xresources
alias merge='xrdb -merge ~/.Xresources'

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

# switch between shells
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"


### RANDOM COLOR SCRIPT ###
# Get this script from my GitLab: gitlab.com/dwt1/shell-color-scripts
# Or install it from the Arch User Repository: shell-color-scripts
#colorscript random
pfetch

eval "$(starship init zsh)"
