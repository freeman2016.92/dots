!  ____ _____ 
! |  _ \_   _|  Derek Taylor (DistroTube)
! | | | || |    http://www.youtube.com/c/DistroTube
! | |_| || |    http://www.gitlab.com/dwt1/ 
! |____/ |_|  	


Xft.dpi: 96
Xft.antialias: true
Xft.hinting: true
Xft.rgba: rgb
Xft.autohint: true
Xft.hintstyle: hintfull
Xft.lcdfilter: lcdfilter

XTerm*renderFont: true
XTerm*faceName: xft:Mononoki Nerd Font, \
                xft:JoyPixels:size=12, \
                xft:Monospace:style=Medium:size=12   
XTerm*faceSize: 11
XTerm*utf8: 2
XTerm*locale: true

XTerm.vt100.translations: #override \n\
  Ctrl <Key> j: smaller-vt-font() \n\
  Ctrl <Key> k: larger-vt-font()


! Every shell is a login shell by default (for inclusion of all necessary environment variables)
XTerm*loginshell: true

! I like a LOT of scrollback...
XTerm*savelines: 16384

! double-click to select whole URLs :D
XTerm*charClass: 33:48,36-47:48,58-59:48,61:48,63-64:48,95:48,126:48

URxvt*imLocale: en_US.UTF-8
URxvt*termName: rxvt-unicode-256color
URxvt*buffered: false
URxvt.intensityStyles: false
URxvt.font: xft:DeJavu Sans Mono:size=11.3, \
            xft:JoyPixels:size=11.4, \
            xft:Monospace:style=Medium:size=11.4   
URxvt.saveLines: 2000
URxvt.scrollBar: false
URxvt.cursorColor: white

! URXVT EXTENSIONS
URxvt.perl-ext-common:            default,clipboard,url-select,tabbedex

! FONT
URxvt.resize-font.step: 1
URxvt.keysym.Control-minus:       resize-font:smaller
URxvt.keysym.Control-plus:        resize-font:bigger
URxvt.keysym.Control-equal:       resize-font:reset
URxvt.keysym.Control-question:    resize-font:show

! CLIPBOARD
URxvt.clipboard.copycmd:          xclip -i -selection clipboard
URxvt.clipboard.pastecmd:         xclip -o -selection clipboard
URxvt.keysym.M-h:                 perl:clipboard:copy
URxvt.keysym.M-j:                 perl:clipboard:paste

! URL
URxvt.keysym.M-u:                 perl:url-select:select_next
URvxt.url-select.button:          2
URxvt.url-select.launcher:        firefox-bin
URxvt.url-select.underline:       true

!!clipboard
!!URxvt.clipboard.autocopy: true
!!URxvt.keysym.Shift-Control-V: eval:paste_clipboard
!!URxvt.keysym.Shift-Control-C: eval:selection_to_clipboard
!!URxvt*depth: 32

! TABBEDEX
URxvt.tabbedex.no-tabbedex-keys:  yes
URxvt.tabbedex.new-button:        false
URXvt.tabbedex.reopen-on-close:   yes
URxvt.tabbedex.autohide:          yes
URxvt.tabbedex.tabbar-fg:         5
URxvt.tabbedex.tabbar-bg:         0
URxvt.tabbedex.tab-fg:            10
URxvt.tabbedex.tab-bg:            0
URxvt.tabbedex.bell-fg:           0
URxvt.tabbedex.bell-bg:           0
URxvt.tabbedex.bell-tab-fg:       0
URxvt.tabbedex.bell-tab-bg:       0
URxvt.tabbedex.title-fg:          14
URxvt.tabbedex.title-bg:          0
URxvt.keysym.Control-Shift-T:     perl:tabbedex:new_tab
URxvt.keysym.Control-Shift-R:     perl:tabbedex:rename_tab
URxvt.keysym.Control-Shift-W:     perl:tabbedex:kill_tab
URxvt.keysym.Control-Next:        perl:tabbedex:next_tab
URxvt.keysym.Control-Prior:       perl:tabbedex:prev_tab
URxvt.keysym.Control-Shift-Next:  perl:tabbedex:move_tab_right
URxvt.keysym.Control-Shift-Prior: perl:tabbedex:move_tab_left

! special
*.foreground:   #c5c8c6
*.background:   #1d1f21
*.cursorColor:  #c5c8c6

! black
*.color0:       #1d1f21
*.color8:       #969896

! red
*.color1:       #cc342b
*.color9:       #cc342b

! green
*.color2:       #198844
*.color10:      #198844

! yellow
*.color3:       #fba922
*.color11:      #fba922

! blue
*.color4:       #3971ed
*.color12:      #3971ed

! magenta
*.color5:       #a36ac7
*.color13:      #a36ac7

! cyan
*.color6:       #3971ed
*.color14:      #3971ed

! white
*.color7:       #c5c8c6
*.color15:      #ffffff

